#!/usr/bin/perl

sub main() {
	my $result = "";
	if ($ARGC < 4) {
		$result = help();
	} else {
		my $module = $ARGV[0];
		my $nInputs = $ARGV[1];
		my $nOutputs = $ARGV[2];
		my $inputfile = $ARGV[3];
		my $clk = ($ARGC > 4 && $ARGV[4] eq "--withClkAndReset");
		
		if ($nInputs !~ /^[0-9]+$/) {
			$result = error('<#inputs> should be a positive integer or zero');
		} elsif ($nOutputs !~ /^[0-9]+$/) {
			$result = error('<#outputs> should be a positive integer');
		} elsif (!(-e "$module.v")) {
			$result = error("$module.v NOT FOUND!");		
		} elsif (!(-e "$inputfile")) {
			$result = error("$inputfile NOT FOUND!");
		} elsif (!$clk && $nInputs == 0) {
			$result = error("NO INPUTS TO GENERATE!");
		} elsif ($nOutputs == 0) {
			$result = error("NO OUTPUTS TO MONITOR!");
		} else {
			open(INPUTS, "<$inputfile");
			my @lines = <INPUTS>;
			close(INPUTS);
			my $nLines = @lines;
			my $duration = $nLines * 20 - 1;
			$result = mock($module, $nInputs, $nOutputs, $inputfile, $duration, $clk);
		}
	}
	print("$result\n");
}

sub help() {
	return '
Verilog AutoMock, Yavuz Koroglu, Feb 2017

Usage: AutoMock.pl <module-name> <#input-ports> <#output-ports> <input-file> [--withClkAndReset]
Example: AutoMock.pl ReferenceComponent 3 1 inputs.txt --withClkAndReset

List of Parameters:
-----------------
  <module-name> 
    Module to mock without .v extension.
		
  <#input-ports>
    Number of input ports excluding clk and rst.

  <#output-ports>
    Number of output ports.
	
  <input-file>
    File that contains test inputs.
	
  --withClkAndReset
    Must be given when the module requires a clock and a reset.
 
NOTE: All modules must define outputs, inputs and clock in respective order.';
}

sub error() {
	return "ERROR: @_";
}

sub mock() {
	my ($moduleName, $nInputs, $nOutputs, $inputfile, $duration, $clk) = @_;
	my $outputGeneratorName = "$moduleName\_OutputGenerator";
	my $inputGeneratorName = "$moduleName\_InputGenerator";
	my $mockName = "$moduleName" . "Mock";
	my $nInputsMinus1 = $nInputs - 1;
	my $nOutputsMinus1 = $nOutputs - 1;
	
	unlink "$mockName.v" if (-e "$mockName.v");
	
	open(CLOCK, ">Clock.v");
	print CLOCK '
// This module;
// => Should NOT be changed and 
// => Should be PRESENT for automated testing.

`timescale 1ns / 1ps
module Clock(clk);

output reg clk = 0;

always #10 clk <= !clk;

endmodule
';
	close(CLOCK);
	
	open(INPUTGENERATOR, ">$inputGeneratorName.v");
	print INPUTGENERATOR '
// This module is automatically generated.

`timescale 1ns / 1ps
' . "module $inputGeneratorName(" . ($nInputs == 0 ? "" : "x, ") . ($clk ? "rst, " : "") . "clk);" . '

' . ($nInputs == 0 ? "" : "output reg [$nInputsMinus1:0] x;") . '
' . ($clk ? "output reg rst;" : "") . '
input wire clk;

integer inputs;
integer unused;

initial begin
' . "	inputs = \$fopen(\"$inputfile\", \"r\");" . '
	if (inputs == 0) begin
		$display("inputs.txt NOT FOUND!");
		$finish;
	end
	
' . "	unused <= \$fscanf(inputs, \"" . ('%b' x $nInputs) . ($clk ? '%b' : '') . '"'; 

	my $i = $nInputs;
	print INPUTGENERATOR ", x[$i]" while ($i-- > 0);
	print INPUTGENERATOR ($clk ? ", rst" : "");
	
	print INPUTGENERATOR ');
end

always @(negedge clk) begin
	if (!$feof(inputs)) begin
' . "		unused <= \$fscanf(inputs, \"" . ('%b' x $nInputs) . ($clk ? '%b' : '') . '"'; 

	my $j = $nInputs;
	print INPUTGENERATOR ", x[$j]" while ($j-- > 0);
	print INPUTGENERATOR ($clk ? ", rst" : "");
	
	print INPUTGENERATOR ');
	end
end

endmodule
';
	close(INPUTGENERATOR);
	
	open(OUTPUTGENERATOR, ">$outputGeneratorName.v");
	print OUTPUTGENERATOR '
// This is the reference tester to generate reference_mock

`timescale 1ns / 1ps
' . "module $outputGeneratorName();" . '

' . ($nInputs == 0 ? "" : "wire [$nInputsMinus1:0] inputs;") . '
' . ($clk ? "wire rst;" : "") . '
' . "wire [$nOutputsMinus1:0] outputs;" . '
wire clk;

Clock globalClock(clk);
' . "$inputGeneratorName inputGen(" . ($nInputs == 0 ? "" : "inputs, ") . ($clk ? "rst, " : "") . "clk);" . '
' . "$moduleName referenceComponent(outputs" . ($nInputs == 0 ? "" : ", inputs") . ($clk ? ", clk, rst" : "") . ");" . '

initial begin
' . "	\$dumpfile(\"$mockName.vcd\");" . '
' . "	\$dumpvars(0, clk, outputs" . ($nInputs == 0 ? "" : ", inputs") . ($clk ? ", rst" : "") . ");" . '
' . "	\$monitor(\"clk = 1'b%b, outputs = $nOutputs\'b" . ('%b' x $nOutputs) . "\", clk, outputs);" . '
' . "	#$duration;" . '
	$finish;
end

endmodule
';
	close(OUTPUTGENERATOR);	
	
	my $err = `iverilog -o $outputGeneratorName.vvp $moduleName.v Clock.v $inputGeneratorName.v $outputGeneratorName.v`;
	return($err) if ($err ne "");

	open(MOCK, ">$mockName.v");	
	print MOCK '
// This is the reference mock generated for the student

`timescale 1ns / 1ps
' . "module $mockName(outputs" . ($nInputs == 0 ? "" : ", inputs") . ($clk ? ", rst, clk" : "") . ");" . '

' . "output reg [$nOutputsMinus1:0] outputs;" . '
' . ($nInputs == 0 ? "" : "input wire [$nInputsMinus1:0] inputs;") . '
' . ($clk ? "input wire clk, rst;\n" : "") . '
initial begin';
	my @lines = `vvp $outputGeneratorName.vvp -fst`;
	
	my $lineNo = 0;
	foreach $line (@lines) {
		$line =~ s/^\s+|\s+$//g;
		
		if($lineNo == 0) {
			return join("", @lines) if ($line !~ /^FST info: dumpfile .+\.vcd opened for output\./);
		} else {
			return join("", @lines) if ($line !~ /^clk.*outputs = ([0-9]'b[0-9|x|z]+)/);
            $line =~ s/clk = (.*), //g;
            if ($1 eq "1'b0") {
                if($lineNo == 1) {
				    print MOCK "\n\t$line;";
                } else {
                    print MOCK "\n\t#20 $line;";
                }
            }
		}
		$line .= "\n";
		
		$lineNo++;
	}
	
	print MOCK '
end

endmodule
';

	close(MOCK);
		
	return "$mockName.v IS CREATED!";
}

$ARGC = @ARGV; 
main();